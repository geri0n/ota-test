#!/usr/bin/env python3

# SPDX-FileCopyrightText: 2021 Gerion Entrup <gerion.entrup@flump.de>
#
# SPDX-License-Identifier: GPL-3.0-or-later
#
# Adapted from https://github.com/julianxhokaxhiu/LineageOTAUnitTest
# and https://github.com/lineageos-infra/updater

"""Simple test script for a Lineage OTA Server."""

import requests
import argparse
import sys

REST_URL = "api/v1"
ROM_TYPE = "nightly"
INC = "1"

parser = argparse.ArgumentParser(
    prog=sys.argv[0],
    description=sys.modules[__name__].__doc__,
    formatter_class=argparse.ArgumentDefaultsHelpFormatter,
)

parser.add_argument(
    "--ota-url", "-o", default="https://download.lineageos.org", help="OTA server URL."
)
parser.add_argument(
    "--device", "-d", default="sailfish", help="Device which is requested."
)
parser.add_argument(
    "--query", "-q", default=None, help="Just query an url."
)
args = parser.parse_args()

ota_url = args.ota_url.strip("/")

headers = {
    "Cache-control": "no-cache",
    "Content-type": "application/json",
    "User-Agent": "com.gitlab.geri0n.ota-test",
}

if args.query:
    response = requests.get(
        "/".join([ota_url, REST_URL, args.query]), headers=headers
    )
    print("Raw response: ", response.text)
    sys.exit(0)

response = requests.get(
    "/".join([ota_url, REST_URL, args.device, ROM_TYPE, INC]), headers=headers
)
print("Raw response: ", response.text)
try:
    assert response.json()["response"][0]["filename"]
except Exception:
    print("Error: Did not found valid data.")
    sys.exit(1)

print("Success: Server seems to work.")
