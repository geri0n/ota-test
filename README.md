LineageOS OTA test script
=========================

Usage:
```
./test.py
./test.py -h
```

Credit
------

Adapted from https://github.com/julianxhokaxhiu/LineageOTAUnitTest
and the API documentation in https://github.com/lineageos-infra/updater.
